#!/data/data/com.termux/files/usr/bin/bash

wasm-pack build
cp p.json pkg/package.json
cd www
rm -rf node_modules/wasm-test && npm i

import { Universe, Cell } from "wasm-test";
import { memory } from "wasm-test/wasm_test_bg";

const cellSize = 5;
const gridColor = "#CCCCCC";
const deadColor = "#FFFFFF";
const aliveColor = "#000000";

const log = (str) => console.log(str)

const universe = Universe.new();
const height = universe.height();
const width = universe.width();

const canvas = document.getElementById("canvas");
const preVariant = document.getElementById("prevariant");


canvas.height = (cellSize+1)*height+1;
canvas.width = (cellSize+1)*width+1;

const ctx = canvas.getContext('2d');

const getIndex = (row, column) => 
	{ return row * width + column; }

const drawGrid = () => {
	ctx.beginPath();
	ctx.strokeStyle = gridColor;

	for(let i=0; i<= width; i++) {
		ctx.moveTo(i*(cellSize+1)+1, 0);
		const lineEnd = (cellSize+1)*height+1;
		ctx.lineTo(i*(cellSize+1)+1, lineEnd)
	}

	for(let i=0; i<=height; i++) {
		const lineStart = (cellSize+1)*width+1;
		const lineEnd = i*(cellSize+1)+1;
		ctx.moveTo(0,lineEnd);
		ctx.lineTo(lineStart, lineEnd);
	}

	ctx.stroke();
}

const drawCells = () =>{
	const cellsPtr = universe.cells();
	const cells = new Uint8Array(memory.buffer, cellsPtr, width * height);

	ctx.beginPath();

	for(let row=0; row < height; row++) {
		for(let col=0; col<width; col++) {
			const idx = getIndex(row,col);

			ctx.fillStyle = cells[idx] === Cell.Dead
				? deadColor
				: aliveColor;

			ctx.fillRect(
				col*(cellSize+1)+1,
				row*(cellSize+1)+1,
				cellSize,
				cellSize
			);
			
		}
	}

	ctx.stroke();
	
}



const renderLoop = () => {
	// preVariant.textContent = universe.render();
	universe.tick();

	drawGrid();
	drawCells();

	requestAnimationFrame(renderLoop);
}

// drawGrid();

requestAnimationFrame(renderLoop);

